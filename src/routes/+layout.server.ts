import type { LayoutServerLoad } from './$types';
import type { PrismicDocument } from "@prismicio/types"
import { error } from "@sveltejs/kit"

import createClient from "$lib/prismic/functionality/client"

export const load = (async ({ fetch, request }) => {
  
    const api = await createClient({ fetch, request })

    try {
      const prismicDocument: PrismicDocument = await api.getSingle('models')

      if (prismicDocument) {        
        return {
          prismicDocument
        };
      }

    } catch (err) {
      console.error(err)
      throw error(404, "Page not found")

    }

}) satisfies LayoutServerLoad;