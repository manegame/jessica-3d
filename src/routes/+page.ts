// import createClient from "$lib/prismic/functionality/client"

export async function load ({ parent }) {
  // const api = await createClient('https://')

  // const doc = await api.getSingle('models')

  return { ...await parent() }
}