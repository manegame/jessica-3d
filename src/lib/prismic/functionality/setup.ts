export const namespace = 'jess-3d' // Change this to your Prismic ID
export const apiEndpoint = 'https://' + namespace + '.cdn.prismic.io/api/v2';

export const routes = [
  {
    type: 'models',
    path: '/',
  }
]

export function linkResolver (doc) {
  if (!doc) return '/'
  if (doc.link_type === 'Web') {
    return doc.url
  } else {
    switch (doc.type) {
      default:
        return '/'
    }
  }
}
