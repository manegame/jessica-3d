import { linkResolver } from "../setup"

export const wrapElements = (element: HTMLElement) => {
  for (let el of element.querySelectorAll('[data-label]')) {
    if (el.parentElement.parentElement.parentElement.nodeName === "UL" || el.parentElement.parentElement.parentElement.nodeName === "OL") {
      el.parentElement?.parentElement?.parentElement.classList.add(el.classList)
    }
    if (el.parentElement.parentElement.nodeName === "UL" || el.parentElement.parentElement.nodeName === "OL") {
      el.parentElement?.parentElement.classList.add(el.classList)
    }
    if (el.parentElement.nodeName === "UL" || el.parentElement.nodeName === "OL") {
      el.parentElement?.parentElement.classList.add(el.classList)
    }
  }
}

export const htmlSerializer = {
  // Top level elements
  heading1: ({ children }: any) => `<h1 class="richText">${children}</h1>`,
  heading2: ({ children }: any) => `<h2 class="richText">${children}</h2>`,
  heading3: ({ children }: any) => `<h3 class="richText">${children}</h3>`,
  heading4: ({ children }: any) => `<h4 class="richText">${children}</h4>`,
  heading5: ({ children }: any) => `<h5 class="richText">${children}</h5>`,
  heading6: ({ children }: any) => `<h6 class="richText">${children}</h6>`,
  paragraph: ({ children }: any) => `<p class="richText">${children}</p>`,
  // Nested elements
  preformatted: ({ node }: any) => `<pre>${JSON.stringify(node.text)}</pre>`,
  strong: ({ children }: any) => `<strong>${children}</strong>`,
  em: ({ children }: any) => `<em>${children}</em>`,
  label: ({ node, children }: any) => {
    return `<span class="${node.data.label}" data-label="${node.data.label}">${children}</span>`
  },
  span: ({ text }: any) => (text ? text : ''),
  // List items
  listItem: ({ children }: any) => `<li>${children}</li>`,
  oListItem: ({ children }: any) => `<li>${children}</li>`,
  // Lists
  list: ({ children }: any) => `<ul>${children}</ul>`,
  oList: ({ children }: any) => `<ol>${children}</ol>`,
  // Image
  image: ({ node }: any) => {
    const linkUrl = node.linkTo ? linkResolver(node.linkTo) : null
    const linkTarget =
      node.linkTo && node.linkTo.target
        ? `target="${node.linkTo.target}" rel="noopener"`
        : ''
    const wrapperClassList = [node.label || '', 'block-img']
    const img = `<img src="${node.url}" alt="${
      node.alt ? node.alt : ''
    }" copyright="${node.copyright ? node.copyright : ''}" />`

    return `
        <p class="${wrapperClassList.join(' ')}">
          ${linkUrl ? `<a ${linkTarget} href="${linkUrl}">${img}</a>` : img}
        </p>
      `
  },
  // Video
  embed: ({ node }: any) => `
        <div data-oembed="${node.oembed.embed_url}"
          data-oembed-type="${node.oembed.type}"
          data-oembed-provider="${node.oembed.provider_name}"
          ${label(node)}>
          ${node.oembed.html}
        </div>
      `,
  // Links
  hyperlink: ({ node, children }: any) => {
    const target = node.data.target
      ? `target="${node.data.target}" rel="noopener"`
      : ''
    const url = linkResolver(node.data)
    return `<a ${target} href="${url}">${children}</a>`
  }
}